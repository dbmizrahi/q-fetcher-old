# Build image
FROM adoptopenjdk/openjdk11:jdk-11.0.8_10-alpine-slim AS builder
USER root
WORKDIR /app
ADD ./ /app/
RUN ./gradlew build

# Main app image
FROM adoptopenjdk/openjdk11:jdk-11.0.8_10-alpine-slim
EXPOSE 8080/tcp
WORKDIR /app
ARG JAR_FILE=/app/build/libs/qfetcher.jar
COPY --from=builder ${JAR_FILE} qfetcher.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","--illegal-access=deny", "-jar","/app/qfetcher.jar"]