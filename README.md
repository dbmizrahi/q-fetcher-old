# Q-Fetcher
## Q-Fetcher project for the home assignment

Provides an API for collecting exam questions from the different sources. Written using [Spring Boot](https://spring.io)        
For API documentation run locally and see [http://localhost:5000/swagger-ui.html](http://localhost:5000/swagger-ui.html)        
or go to the deployed demo API documentation [https://2jr3roi3c9.execute-api.us-east-2.amazonaws.com/dev/swagger-ui.html](https://2jr3roi3c9.execute-api.us-east-2.amazonaws.com/dev/swagger-ui.html)       
See the simple test UI application here [https://q-fetcher.mizrahi.co](https://q-fetcher.mizrahi.co). Written using [ReactJS](https://reactjs.org/) 

![img](https://q-uploader.s3.amazonaws.com/q-fetcher-diag.png)

Use swagger to test your own sources:
* upload the image with the scanned text to your public cloud
* upload your json file with the questions in the following format
```aidl
{
    questions: [
        {
            id: 1,
            text: "To be or not to be?",
            field: "literature"
        },
        {
            id: 2,
            text: "What's the gist, physicist?",
            field: "physics"
        }
    ]
}
```
* upload your csv with the questions in the following format
```aidl
id,text,field
```
* create the `manifest.dat` file with the new line separated urls to the uploaded files
* go to the deployed swagger link above, open the endpoint POST /api/v1/fetch, 
  press the button `Try it out` and send the request in the following format
```aidl
{
  "filter": [
    // use here the file extensions as "jpg", "json", "csv" etc. or just do not use the filter
  ],
  "manifest": "https://<your_host_name>/manifest.dat"
}
```

# Getting started

## Prerequisites

* Java 11 SDK (OpenJDK)
* docker

## Building

```
cd qfetcher
./gradlew build
```

# Running the tests

The following command will run all tests:

```
./gradlew test
```

## Test coverage

Each test run is accompanied by a code test coverage report which can be used for the CI/CD pipelines or checked manually using HTML report.        
Current test coverage of instructions is 86%. The total threshold for the coverage is 70%.       
To view the HTML report open this file:
```
└── build
    ├── ...
    └── jacoco
        ├── ...
        └── html
            ├── ...
            └── index.html
```

# Configuration

The following configuration properties may or need to be specified to run the service (may set as an environment variables):

## Google Cloud Vision API properties
* `cloud.vision.key: ${CLOUD_VISION_KEY}` -- API Key for cloud vision (required)
* `cloud.vision.key: ${CLOUD_VISION_URL:https://vision.googleapis.com/v1/images:annotate?key=}` -- cloud vision API url (has default value)

## Application properties
* `web.client.max-in-memory-size: ${MAX_IN_MEMORY_SIZE:16}` -- max in memory size for the cloud vision responses processing (has default value 16MB)
* `management.endpoints.web.cors.allowed-origins: ${ALLOWED_ORIGINS:*}` -- CORS allowed origins (by default any)

## Local run with an IDE or Gradle Wrapper
Set up the environment variable named `CLOUD_VISION_KEY="<api_key>"` with the Google API key
and run code from your IDE or do 
```
export CLOUD_VISION_KEY=<api_key>
cd qfetcher
./gradlew bR
```

## Local run with docker

```
docker build -t qfetcher .
docker run -p 8080:8080 --env CLOUD_VISION_KEY=<api_key> -d qfetcher
```
(additional environment variables may set in time of docker run)

# Development

## Add new WireMock stubs for the Integration Tests

Since the tests uses the @AutoConfigureWireMock annotation in the AbstractTest the prepared stubs stored as a JSON files in the special folder with path `./src/test/resources/mappings`.
The new mappings can be added using the presented format or recorded using [WireMock standalone](http://wiremock.org/docs/running-standalone/)
* Start the standalone WireMock using --port or/and --https-port parameter other than the defined in tests.
* Change the required URL in the section `@SpringBootTest( ... properties = {...})` of the AbstractTest using this port.
* Open the [http://localhost:<standalone_port>/__admin/recorder/](http://localhost:<standalone_port>/__admin/recorder/), fill the outer service URL that response need to be stubbed and click `Record`
* Run the new one specific test
* Stop recording the mappings
* Make sure the mappings are *not* persistent and set in all recorded stub JSONs the field `persistent` as `false`
* Copy recorded JSON file into the mappings folder
* Edit the mapping record name and content for not to use the sensitive data as eg. real API Key etc. according to the dummy data used in tests currently

## Project structure
```
.
├── build.gradle
├── Dockerfile
├── gradle
│   └── wrapper
│       ├── gradle-wrapper.jar
│       └── gradle-wrapper.properties
├── gradlew
├── gradlew.bat
├── lombok.config
├── README.md
├── settings.gradle
└── src
    ├── main
    │   ├── java
    │   │   └── co
    │   │       └── mizrahi
    │   │           └── qfetcher
    │   │               ├── config
    │   │               │   ├── CorsConfig.java
    │   │               │   ├── SwaggerConfig.java
    │   │               │   └── WebClientConfig.java
    │   │               ├── controllers
    │   │               │   └── QFetcherController.java
    │   │               ├── exception
    │   │               │   ├── QFetcherExceptionHandler.java
    │   │               │   ├── QFetcherExceptionInfo.java
    │   │               │   └── QFetcherException.java
    │   │               ├── model
    │   │               │   ├── InputQuestion.java
    │   │               │   ├── InputQuestionList.java
    │   │               │   ├── Question.java
    │   │               │   ├── QuestionList.java
    │   │               │   └── RequestObject.java
    │   │               ├── QFetcherApp.java
    │   │               ├── services
    │   │               │   ├── CsvTextProcessor.java
    │   │               │   ├── ImageTextProcessor.java
    │   │               │   ├── JsonTextProcessor.java
    │   │               │   ├── QFetcherService.java
    │   │               │   ├── TextProcessor.java
    │   │               │   └── TextProcessorType.java
    │   │               └── utils
    │   │                   └── QFetcherUtil.java
    │   └── resources
    │       └── application.yml
    └── test
        ├── java
        │   └── co
        │       └── mizrahi
        │           └── qfetcher
        │               ├── controllers
        │               │   └── QFetcherControllerTest.java
        │               ├── QFetcherAbstractTest.java
        │               ├── QFetcherAppIT.java
        │               ├── services
        │               │   ├── CsvTextProcessorTest.java
        │               │   ├── ImageTextProcessorTest.java
        │               │   ├── JsonTextProcessorTest.java
        │               │   └── QFetcherServiceTest.java
        │               └── utils
        │                   └── QFetcherUtilTest.java
        └── resources
            └── mappings
                ├── ...
```
