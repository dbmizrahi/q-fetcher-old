package co.mizrahi.qfetcher.config;

import io.swagger.annotations.SwaggerDefinition;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;

/**
 * Configuration of the Swagger API documentation.
 * @author David Mizrahi <david@mizrahi.co>
 */
@Slf4j
@Configuration
@EnableSwagger2
@SwaggerDefinition(schemes = {SwaggerDefinition.Scheme.HTTPS, SwaggerDefinition.Scheme.HTTP})
@Import(BeanValidatorPluginsConfiguration.class)
public class SwaggerConfig {

    @Value("${springfox.documentation.swagger.v2.host}")
    private String host;

    @Value("${info.application.version}")
    private String projectVersion;

    @Value("${info.application.name}")
    private String projectName;

    @Value("${info.application.description}")
    private String projectDescription;

    @Bean
    public Docket api() {
        log.info("Starting Swagger");
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("v1")
                .host(this.host)
                .protocols(Set.of("HTTPS", "HTTP"))
                .select()
                .apis(RequestHandlerSelectors
                        .basePackage("co.mizrahi.qfetcher"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(metaInfo());
    }

    private ApiInfo metaInfo(){
        return new ApiInfo(
                projectName,
                projectDescription,
                projectVersion,
                "",
                new Contact("David Mizrahi",
                        "",
                        "david@mizrahi.co"),
                "", "",
                new ArrayList<>()
        );
    }
}
