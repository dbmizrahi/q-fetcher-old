package co.mizrahi.qfetcher.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Configuration of the CORS policy for the service.
 * Allows externally set list of allowed origins using environment variable 'ALLOWED_ORIGINS'.
 * @author David Mizrahi <david@mizrahi.co>
 */
@Slf4j
@Configuration
public class CorsConfig {

    @Value("${management.endpoints.web.cors.allowed-origins}")
    private ArrayList<String> origins;

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        log.info(String.format("CORS trusted origins: %s", Arrays.toString(origins.toArray())));
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(@SuppressWarnings("NullableProblems") CorsRegistry registry) {
                registry.addMapping("/**")
                        .allowedMethods("GET", "POST", "PUT", "PATCH", "DELETE")
                        .allowedOrigins(origins.toArray(new String[0]));
            }
        };
    }
}
