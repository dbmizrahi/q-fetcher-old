package co.mizrahi.qfetcher.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;

/**
 * Configuration of the WebClient for outbound http calls.
 * @author David Mizrahi <david@mizrahi.co>
 */
@Configuration
public class WebClientConfig {

    /**
     * Maximal in memory size in megabytes. By default equals 16.
     * Required for requests to Google Cloud Vision API
     */
    @Value("${web.client.max-in-memory-size}")
    private int maxInMemorySize;

    private static final int MEGABYTE = 1024 * 1024;

    @Bean
    public WebClient client() {
        return WebClient.builder().exchangeStrategies(ExchangeStrategies.builder()
                .codecs(configurer -> configurer.defaultCodecs()
                        .maxInMemorySize(maxInMemorySize * MEGABYTE))
                .build())
                .build();
    }
}
