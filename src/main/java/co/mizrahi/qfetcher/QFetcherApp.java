package co.mizrahi.qfetcher;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QFetcherApp {
    public static void main(String[] args) {
        SpringApplication.run(QFetcherApp.class, args);
    }
}
