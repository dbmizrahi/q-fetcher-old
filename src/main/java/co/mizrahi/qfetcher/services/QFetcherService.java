package co.mizrahi.qfetcher.services;

import co.mizrahi.qfetcher.controllers.QFetcherController;
import co.mizrahi.qfetcher.exception.QFetcherException;
import co.mizrahi.qfetcher.model.Question;
import co.mizrahi.qfetcher.model.QuestionList;
import co.mizrahi.qfetcher.model.RequestObject;
import co.mizrahi.qfetcher.utils.QFetcherUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.io.*;
import java.net.URL;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

/**
 * Main service for processing controller calls.
 * @author David Mizrahi <david@mizrahi.co>
 */
@Log4j2
@Service
@RequiredArgsConstructor
public class QFetcherService {

    @Value("${spring.mvc.async.request-timeout}")
    private Long timeout;

    /**
     * Map with injects all services which implemented the {@link TextProcessor}
     */
    public final Map<String, TextProcessor> processors;

    /**
     * {@link WebClient} bean
     */
    public final WebClient webClient;

    /**
     * Defines which implementation of the {@link TextProcessor} will be used for processing
     * and returns required bean's instance.
     * @param url of the future call to the 3rd party service which contains the
     * file extension that will be used as a key.
     * @return {@link TextProcessor} implementation
     */
    public TextProcessor getProcessor(String url) {
        String extension = url.substring(url.lastIndexOf(".") + 1);
        String processorType = TextProcessorType.find(extension).toString();
        return this.processors.get(processorType);
    }

    /**
     * Makes processing and collecting the data received from 3rd party services
     * and returns to {@link QFetcherController} DTO for the response body.
     * @param requestObject {@link RequestObject}
     * @return {@link QuestionList}
     */
    public QuestionList fetch(RequestObject requestObject) {
        List<String> manifestUrls = this.getManifestUrls(requestObject.getManifest());
        Predicate<String> stringPredicate = url -> true;
        List<String> filter = requestObject.getFilter();
        if (nonNull(filter) && !filter.isEmpty()) {
            if (filter.contains("jpg")) filter.add("jpeg");
            stringPredicate = url -> filter.stream().anyMatch(url::contains);
        }
        List<Question> questions = Objects.requireNonNull(manifestUrls)
                .parallelStream()
                .filter(stringPredicate)
                .map(url -> {
                    try {
                        return this.getQuestions(url).get(timeout, TimeUnit.MILLISECONDS);
                    } catch (InterruptedException | ExecutionException | TimeoutException e) {
                        return List.of(Question.builder().value("Data processing failed").source(QFetcherUtil.getExtension(url)).build());
                    }
                })
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
        return QuestionList.builder().questions(questions).build();
    }

    /**
     * Makes a call to the chosen {@link TextProcessor}
     * @param url for the call to 3rd party service
     * @return list of {@link Question}
     */
    public CompletableFuture<List<Question>> getQuestions(String url) {
        TextProcessor processor = this.getProcessor(url);
        return processor.processQuestions(url, webClient);
    }

    /**
     * Extracts lines from the .dat file
     * @param manifest {@link String}
     * @return list of extracted strings of url's
     */
    public List<String> getManifestUrls(String manifest) {
        try {
            InputStream inputStream = new URL(manifest).openStream();
            return new BufferedReader(
                    new InputStreamReader(inputStream))
                    .lines()
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new QFetcherException(HttpStatus.BAD_REQUEST, "Manifest data malformed or not accessible");
        }
    }
}
