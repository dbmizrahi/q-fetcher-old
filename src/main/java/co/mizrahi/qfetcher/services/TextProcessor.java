package co.mizrahi.qfetcher.services;

import co.mizrahi.qfetcher.model.Question;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * Interface for the incoming data processors.
 * @author David Mizrahi <david@mizrahi.co>
 */
public interface TextProcessor {
    CompletableFuture<List<Question>> processQuestions(String url, WebClient restTemplate);
}
