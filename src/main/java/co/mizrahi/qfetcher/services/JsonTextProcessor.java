package co.mizrahi.qfetcher.services;

import co.mizrahi.qfetcher.model.InputQuestionList;
import co.mizrahi.qfetcher.model.Question;
import co.mizrahi.qfetcher.utils.QFetcherUtil;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

/**
 * Implementation of the {@link TextProcessor}
 * for processing the JSON files
 * @author David Mizrahi <david@mizrahi.co>
 */
@Service("JSON")
public class JsonTextProcessor implements TextProcessor {

    /**
     * @param url for the call to 3rd party service
     * @param client {@link WebClient}
     * @return list of {@link Question}
     */
    @Override
    @Async("executor")
    public CompletableFuture<List<Question>> processQuestions(String url, WebClient client) {
        InputQuestionList inputQuestionList = client.get()
                .uri(url)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .retrieve().bodyToMono(InputQuestionList.class).block();
        return CompletableFuture.completedFuture(this.getQuestions(inputQuestionList));
    }

    List<Question> getQuestions(InputQuestionList inputQuestionList) {
        return Objects.requireNonNull(inputQuestionList)
                .getQuestions()
                .stream()
                .map(q -> QFetcherUtil.convert(q, "json"))
                .collect(Collectors.toList());
    }
}
