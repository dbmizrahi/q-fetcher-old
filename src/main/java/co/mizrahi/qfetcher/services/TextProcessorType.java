package co.mizrahi.qfetcher.services;

import java.util.List;

/**
 * Enum for the bean names detection
 */
public enum TextProcessorType {

    CSV("csv"),
    JSON("json"),
    IMG("png", "jpg", "jpeg", "tiff", "bmp", "pdf", "raw");

    public final List<String> values;

    TextProcessorType(String... values) {
        this.values = List.of(values);
    }

    public List<String> getValues() {
        return this.values;
    }

    public static TextProcessorType find(String extension) {
        return List.of(TextProcessorType.values()).stream()
                .filter(t -> t.getValues().contains(extension))
                .findFirst()
                .orElse(null);
    }
}
