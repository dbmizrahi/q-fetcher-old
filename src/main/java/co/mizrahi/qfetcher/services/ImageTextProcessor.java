package co.mizrahi.qfetcher.services;

import co.mizrahi.qfetcher.exception.QFetcherException;
import co.mizrahi.qfetcher.model.Question;
import co.mizrahi.qfetcher.utils.QFetcherUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.util.retry.Retry;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.CompletableFuture;

import static java.util.Objects.nonNull;

/**
 * Implementation of the {@link TextProcessor}
 * for processing the image files
 * @author David Mizrahi <david@mizrahi.co>
 */
@Service("IMG")
public class ImageTextProcessor implements TextProcessor {

    @Value("${cloud.vision.url}")
    private String cloudVisionUrl;

    @Value("${cloud.vision.key}")
    private String apiKey;

    /**
     * @param url for the call to 3rd party service
     * @param client {@link WebClient}
     * @return list of {@link Question}
     */
    @Override
    @Async("executor")
    public CompletableFuture<List<Question>> processQuestions(String url, WebClient client) {
        Object responseBody;
        try {
            responseBody = this.getParsedText(url, client);
        } catch (IOException e) {
            responseBody = this.getFailedResponseBody();
        }
        return CompletableFuture.completedFuture(this.getQuestions(url, responseBody));
    }

    @SuppressWarnings("unchecked")
    List<Question> getQuestions(String url, Object responseBody) {
        String text = null;
        if (nonNull(responseBody)) {
            text = Optional.ofNullable((((Map<String, List<Map<String, Map<String, String>>>>) responseBody)
                    .get("responses"))
                    .get(0)
                    .get("fullTextAnnotation")
                    .get("text")).orElseThrow(() -> new QFetcherException(HttpStatus.INTERNAL_SERVER_ERROR,
                    "Image cannot be processed or Cloud Vision service response malformed"));
        }
        String source = QFetcherUtil.getExtension(url);
        String result = text.replace("\n", " ");
        return List.of(Question.builder().value(result).source(source).build());
    }

    /**
     * Fetches required text from the cloud vision service
     * @param url for the formation of the request body
     * @param client {@link WebClient}
     * @return received object or throws exception if failed
     */
    private Object getParsedText(String url, WebClient client) throws IOException {
        Map<String, Object> requestBody = this.getRequestBody(url);
        Mono<Object> objectMono = client.post()
                    .uri(cloudVisionUrl + apiKey)
                    .body(Mono.just(requestBody), Object.class)
                    .retrieve()
                    .bodyToMono(Object.class)
                    .retryWhen(Retry.fixedDelay(5, Duration.ofSeconds(1)));
        return objectMono.block();
    }

    /**
     * Makes formation of the response body of the cloud vision service
     * in case if call failed
     * @return complex map
     */
    private Map<String, List<Map<String, Map<String, String>>>> getFailedResponseBody() {
        return Map.of("responses", List.of(
                Map.of("fullTextAnnotation",
                        Map.of("text", "Image cannot be processed")
                )
            )
        );
    }

    /**
     * Makes formation of the request body for the call to cloud vision service
     * @param url for the formation of the request body
     * @return complex map
     */
    private Map<String, Object> getRequestBody(String url) throws IOException {
        InputStream inputStream = new URL(url).openStream();
        String encodedString = Base64.getEncoder().encodeToString(inputStream.readAllBytes());
        return Map.of("requests", List.of(
                Map.of(
                        "image", Map.of("content", encodedString),
                        "features", List.of(Map.of("type", "TEXT_DETECTION"))
                )
            )
        );
    }
}
