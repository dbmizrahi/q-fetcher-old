package co.mizrahi.qfetcher.services;

import co.mizrahi.qfetcher.model.InputQuestion;
import co.mizrahi.qfetcher.model.Question;
import co.mizrahi.qfetcher.utils.QFetcherUtil;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.CsvToBeanBuilder;
import lombok.SneakyThrows;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.io.*;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

/**
 * Implementation of the {@link TextProcessor}
 * for processing the CSV files
 * @author David Mizrahi <david@mizrahi.co>
 */
@Service("CSV")
public class CsvTextProcessor implements TextProcessor {

    /**
     * @param url for the call to 3rd party service
     * @param client {@link WebClient}
     * @return list of {@link Question}
     */
    @SneakyThrows
    @Override
    @Async("executor")
    public CompletableFuture<List<Question>> processQuestions(String url, WebClient client) {
        String body = client.get()
                .uri(url)
                .header("Content-Type", "text/csv")
                .retrieve().bodyToMono(String.class).block();
        List<Object> listQuestions = getQuestions(body);
        return CompletableFuture.completedFuture(listQuestions.stream()
                .map(q -> QFetcherUtil.convert((InputQuestion) q, "csv"))
                .collect(Collectors.toList()));
    }

    List<Object> getQuestions(String body) throws IOException {
        Reader reader = new StringReader(Objects.requireNonNull(body));
        ColumnPositionMappingStrategy<InputQuestion> ms = new ColumnPositionMappingStrategy<>();
        ms.setType(InputQuestion.class);
        List<Object> listQuestions = new CsvToBeanBuilder<>(reader)
                .withType(InputQuestion.class)
                .withMappingStrategy(ms)
                .withSkipLines(1)
                .build().parse();
        reader.close();
        return listQuestions;
    }
}
