package co.mizrahi.qfetcher.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Data transfer model for outgoing responses
 * for the list of {@link Question}
 * @author David Mizrahi <david@mizrahi.co>
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter @Setter @Builder
@ApiModel(value = "QuestionList", description = "Outbound DTO for the list of Questions")
public class QuestionList {

    @Builder.Default
    @ApiModelProperty(name = "questions", dataType = "List<Question>", notes = "List of questions")
    List<Question> questions = new ArrayList<>();
}
