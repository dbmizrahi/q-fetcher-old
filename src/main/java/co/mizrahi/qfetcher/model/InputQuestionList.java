package co.mizrahi.qfetcher.model;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Data access model for the list of {@link InputQuestion}
 * @author David Mizrahi <david@mizrahi.co>
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class InputQuestionList {
    List<InputQuestion> questions = new ArrayList<>();
}
