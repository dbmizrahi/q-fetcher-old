package co.mizrahi.qfetcher.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Data transfer model for incoming responses
 * @author David Mizrahi <david@mizrahi.co>
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter @Setter @Builder
@ApiModel(value = "RequestObject", description = "Inbound DTO for the request data")
public class RequestObject {

    @ApiModelProperty(name = "manifest", dataType = "String", notes = "Link to the manifest.dat file")
    String manifest;

    @Builder.Default
    @ApiModelProperty(name = "filter", dataType = "List<String>", notes = "List of filtering keys (file extensions)")
    List<String> filter = new ArrayList<>();

    @Override
    public String toString() {
        return "RequestObject{" +
                "manifest='" + manifest + '\'' +
                ", filter=" + filter +
                '}';
    }
}
