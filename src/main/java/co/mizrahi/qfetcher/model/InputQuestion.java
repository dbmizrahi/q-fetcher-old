package co.mizrahi.qfetcher.model;

import com.opencsv.bean.CsvBindByPosition;
import lombok.*;

/**
 * Data access model for JSON and CSV data
 * @author David Mizrahi <david@mizrahi.co>
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter @Setter @Builder
public class InputQuestion {

    @CsvBindByPosition(position = 0)
    Long id;

    @CsvBindByPosition(position = 1)
    String text;

    @CsvBindByPosition(position = 2)
    String field;
}
