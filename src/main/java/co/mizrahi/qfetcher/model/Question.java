package co.mizrahi.qfetcher.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

/**
 * Data transfer model for outgoing responses
 * @author David Mizrahi <david@mizrahi.co>
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter @Setter @Builder @EqualsAndHashCode
@ApiModel(value = "Question", description = "Outbound DTO for the single Question")
public class Question {

    @ApiModelProperty(name = "value", dataType = "String", notes = "Value of the question")
    String value;

    @ApiModelProperty(name = "source", dataType = "String", notes = "Source file extension of the question")
    String source;
}
