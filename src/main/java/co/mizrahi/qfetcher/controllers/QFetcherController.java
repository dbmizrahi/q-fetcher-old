package co.mizrahi.qfetcher.controllers;

import co.mizrahi.qfetcher.model.QuestionList;
import co.mizrahi.qfetcher.model.RequestObject;
import co.mizrahi.qfetcher.services.QFetcherService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.Callable;

/**
 * Main REST Controller.
 * API documented with Swagger's springfox library.
 * @author David Mizrahi <david@mizrahi.co>
 */
@Log4j2
@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
@Api(tags = "QFetcherController for fetching the list of questions")
public class QFetcherController {

    private final QFetcherService qFetcherService;

    @PostMapping("/fetch")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(
            value = "Fetch list of questions",
            response = QuestionList.class,
            notes = "API calls to this endpoint will be timed out for the period set in milliseconds " +
                    "using environment variable 'REQUEST_TIMEOUT'"
    )
    public Callable<QuestionList> fetch(
            @ApiParam(required = true, value = "RequestObject")
            @RequestBody RequestObject requestObject) {
        log.info(requestObject.toString());
        return () -> this.qFetcherService.fetch(requestObject);
    }
}
