package co.mizrahi.qfetcher.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

import javax.ws.rs.BadRequestException;

/**
 * QFetcherException for custom error handling client application's bad requests.
 * Extends {@link BadRequestException}
 * @author David Mizrahi <david@mizrahi.co>
 */
@Getter
public class QFetcherException extends BadRequestException {

    private final String reason;
    private final HttpStatus status;

    public QFetcherException(HttpStatus status, String reason) {
        this.status = status;
        this.reason = reason;
    }

    @SuppressWarnings("WeakerAccess")
    public HttpStatus getHttpStatus() {
        return this.status;
    }
}
