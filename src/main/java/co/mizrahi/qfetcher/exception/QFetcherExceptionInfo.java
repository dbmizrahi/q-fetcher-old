package co.mizrahi.qfetcher.exception;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

import static java.util.Objects.isNull;

/**
 * Custom exception response body.
 * @author David Mizrahi <david@mizrahi.co>
 */
@Data
@NoArgsConstructor
public class QFetcherExceptionInfo {

    LocalDateTime timestamp;
    int status;
    String error;
    String path;
    String message;

    @JsonIgnore
    QFetcherException e;

    public QFetcherExceptionInfo(QFetcherException e, HttpStatus status, String path){
        this.e = e;
        this.timestamp = LocalDateTime.now();
        this.path = path;
        this.message = isNull(e.getReason()) ? e.getMessage() : e.getReason();
        this.status = status.value();
        this.error = status.getReasonPhrase();
    }

    public QFetcherExceptionInfo(QFetcherException e, String path){
        this.e = e;
        this.timestamp = LocalDateTime.now();
        this.path = path;
        this.message = isNull(e.getReason()) ? e.getMessage() : e.getReason();
        this.status = e.getHttpStatus().value();
        this.error = e.getHttpStatus().getReasonPhrase();
    }
}
