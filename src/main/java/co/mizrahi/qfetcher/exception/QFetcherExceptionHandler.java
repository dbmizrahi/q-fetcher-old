package co.mizrahi.qfetcher.exception;

import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.async.AsyncRequestTimeoutException;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import org.springframework.web.servlet.resource.ResourceHttpRequestHandler;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

import static java.util.Objects.nonNull;

/**
 * Custom exception handler.
 * Extends {@link ResourceHttpRequestHandler}
 * @author David Mizrahi <david@mizrahi.co>
 */
@Log4j2
@ControllerAdvice
public class QFetcherExceptionHandler extends ResourceHttpRequestHandler {

    /**
     * Handles exception and returns http response entity with an error data
     * in case if client application sent bad request.
     * @param req {@link HttpServletRequest}
     * @param ex  {@link QFetcherException}
     * @return {@link ResponseEntity}
     */
    @ExceptionHandler({QFetcherException.class})
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<QFetcherExceptionInfo> handleQFetcherException(HttpServletRequest req, QFetcherException ex) {
        return new ResponseEntity<>(new QFetcherExceptionInfo(ex, ex.getStatus(), req.getServletPath()), ex.getHttpStatus());
    }

    /**
     * Handles exception and returns {@link QFetcherExceptionInfo} with an error data
     * in case if incoming request contains data which may cause {@link IOException}
     * e.g. incorrect manifest link or malformed manifest.dat file retrieved by that link.
     * @param req {@link HttpServletRequest}
     * @param ex  {@link IOException}
     * @return {@link QFetcherExceptionInfo}
     */
    @ExceptionHandler({IOException.class})
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public QFetcherExceptionInfo handle(HttpServletRequest req, IOException ex) {
        return new QFetcherExceptionInfo(new QFetcherException(HttpStatus.BAD_REQUEST, ex.getMessage()), req.getServletPath());
    }

    /**
     * Handles exception and returns {@link QFetcherExceptionInfo} with an error data
     * in case if incoming request contains data which may cause {@link MethodArgumentNotValidException}
     * or request parameter, path variable or request body is missing.
     * @param req {@link HttpServletRequest}
     * @param ex  {@link MethodArgumentNotValidException}
     * @return {@link QFetcherExceptionInfo}
     */
    @ExceptionHandler({MethodArgumentNotValidException.class})
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public QFetcherExceptionInfo handle(HttpServletRequest req, MethodArgumentNotValidException ex) {
        return new QFetcherExceptionInfo(new QFetcherException(HttpStatus.BAD_REQUEST, ex.getMessage()), req.getServletPath());
    }

    /**
     * Handles exception and returns {@link QFetcherExceptionInfo} with an error data
     * in case is response from third-party service is not readable.
     * @param req {@link HttpServletRequest}
     * @param ex  {@link HttpMessageNotReadableException}
     * @return {@link ResponseEntity}
     */
    @ExceptionHandler({HttpMessageNotReadableException.class})
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public QFetcherExceptionInfo handle(HttpServletRequest req, HttpMessageNotReadableException ex) {
        String message = ex.getMessage();
        String exceptionMessage = nonNull(message) ? message.substring(0, message.indexOf(":")) : "Bad Request";
        return new QFetcherExceptionInfo(new QFetcherException(HttpStatus.BAD_REQUEST, exceptionMessage), req.getServletPath());
    }
    /**
     * Handles exception and returns http response entity with an error data
     * in case of {@link WebClient} responded with an exception.
     * @param req {@link HttpServletRequest}
     * @param ex  {@link WebClientResponseException}
     * @return {@link ResponseEntity}
     */
    @ExceptionHandler({WebClientResponseException.class})
    @ResponseBody
    public ResponseEntity<QFetcherExceptionInfo> handle(HttpServletRequest req, WebClientResponseException ex) {
        log.error(ex.getMessage());
        return new ResponseEntity<>(
                new QFetcherExceptionInfo(
                        new QFetcherException(ex.getStatusCode(), ex.getMessage()), req.getServletPath()),
                ex.getStatusCode());
    }

    /**
     * Handles {@link AsyncRequestTimeoutException} and returns http response entity with an error data
     * in case if third-party service takes too long to process the request or is unavailable
     * and environment variable 'REQUEST_TIMEOUT' was set for less time in milliseconds.
     * @param req {@link HttpServletRequest}
     * @return {@link QFetcherExceptionInfo}
     */
    @ExceptionHandler({AsyncRequestTimeoutException.class})
    @ResponseBody
    @ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
    public QFetcherExceptionInfo handle(HttpServletRequest req) {
        String message = "The third-party service takes too long to process the request or is unavailable";
        return new QFetcherExceptionInfo(new QFetcherException(HttpStatus.SERVICE_UNAVAILABLE, message), req.getServletPath());
    }
}
