package co.mizrahi.qfetcher.utils;

import co.mizrahi.qfetcher.model.InputQuestion;
import co.mizrahi.qfetcher.model.Question;

/**
 * Simple class for utils static methods
 */
public class QFetcherUtil {
    public static Question convert(InputQuestion inputQuestion, String source) {
        return Question.builder()
                .value(inputQuestion.getText())
                .source(source)
                .build();
    }

    public static String getExtension(String url) {
        return url.substring(url.lastIndexOf(".") + 1);
    }
}
