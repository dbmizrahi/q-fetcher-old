package co.mizrahi.qfetcher.utils;

import co.mizrahi.qfetcher.model.InputQuestion;
import co.mizrahi.qfetcher.model.Question;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class QFetcherUtilTest {

    @Test
    void convert() {
        String source = "json";
        String text = "text";
        Question convert = QFetcherUtil.convert(
                InputQuestion.builder()
                        .id(1L)
                        .field("field")
                        .text(text)
                        .build(), source);
        assertEquals(convert.getSource(), source);
        assertEquals(convert.getValue(), text);
    }
}