package co.mizrahi.qfetcher.services;

import co.mizrahi.qfetcher.model.Question;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class ImageTextProcessorTest {

    @InjectMocks
    private ImageTextProcessor imageTextProcessor;

    @Test
    void getQuestions$failedWithEmptyResponse() {
        assertThrows(ClassCastException.class, () -> imageTextProcessor.getQuestions("", new Object()));
    }

    @Test
    void getQuestions() {
        String url = "https://localhost:8000/qsnapshot.png";
        String text = "text";
        Map<String, List<Map<String, Map<String, String>>>> responses =
                Map.of("responses", List.of(Map.of("fullTextAnnotation", Map.of(text, text))));
        List<Question> questions = imageTextProcessor.getQuestions(url, responses);
        Question question = questions.get(0);
        Question questionForAssertion = Question.builder().source("png").value(text).build();
        assertEquals(question.getSource(), questionForAssertion.getSource());
        assertEquals(question.getValue(), questionForAssertion.getValue());
    }
}