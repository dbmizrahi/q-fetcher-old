package co.mizrahi.qfetcher.services;

import co.mizrahi.qfetcher.model.InputQuestion;
import co.mizrahi.qfetcher.model.Question;
import co.mizrahi.qfetcher.utils.QFetcherUtil;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class CsvTextProcessorTest {

    @InjectMocks
    private CsvTextProcessor csvTextProcessor;

    @Test
    void getQuestions() throws IOException {
        String field = "field";
        String text = "text";
        String body = String.format("id,text,field\n1,%s,%s", text, field);
        List<Object> questions = csvTextProcessor.getQuestions(body);
        Question question = questions.stream()
                .map(q -> QFetcherUtil.convert((InputQuestion) q, "csv"))
                .collect(Collectors.toList()).get(0);
        Question questionForAssertion = Question.builder().source("csv").value(text).build();
        assertEquals(question, questionForAssertion);
    }
}