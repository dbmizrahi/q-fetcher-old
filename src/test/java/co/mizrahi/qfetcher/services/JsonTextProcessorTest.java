package co.mizrahi.qfetcher.services;

import co.mizrahi.qfetcher.model.InputQuestion;
import co.mizrahi.qfetcher.model.InputQuestionList;
import co.mizrahi.qfetcher.model.Question;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class JsonTextProcessorTest {

    @InjectMocks
    private JsonTextProcessor jsonTextProcessor;

    @Test
    void getQuestions() {
        String field = "field";
        String text = "text";
        InputQuestionList inputQuestionsLis = new InputQuestionList(List.of(InputQuestion.builder().id(1L).field(field).text(text).build()));
        List<Question> questions = jsonTextProcessor.getQuestions(inputQuestionsLis);
        Question question = questions.get(0);
        Question questionForAssertion = Question.builder().source("json").value(text).build();
        assertEquals(question, questionForAssertion);
    }
}