package co.mizrahi.qfetcher.services;

import co.mizrahi.qfetcher.exception.QFetcherException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class QFetcherServiceTest {

    @Mock
    private CsvTextProcessor csvTextProcessor;

    @Mock
    private JsonTextProcessor jsonTextProcessor;

    @Mock
    private ImageTextProcessor imageTextProcessor;

    @Mock
    private Map<String, TextProcessor> processorMap;

    @Mock
    private WebClient webClient;

    @InjectMocks
    private QFetcherService qFetcherService;

    @Test
    void getManifestUrls() {
        assertThrows(QFetcherException.class, () -> qFetcherService.getManifestUrls(""));
    }

}