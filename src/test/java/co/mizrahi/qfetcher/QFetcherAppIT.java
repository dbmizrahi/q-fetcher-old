package co.mizrahi.qfetcher;

import co.mizrahi.qfetcher.model.RequestObject;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.stream.Stream;

import static org.apache.http.HttpStatus.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class QFetcherAppIT extends QFetcherAbstractTest {

    private Stream<Arguments> filter() {
        return Stream.of(
                Arguments.of(List.of()),
                Arguments.of(List.of("json")),
                Arguments.of(List.of("csv")),
                Arguments.of(List.of("png")),
                Arguments.of(List.of("json", "csv")),
                Arguments.of(List.of("json", "png")),
                Arguments.of(List.of("csv", "png"))
        );
    }

    @ParameterizedTest
    @MethodSource("filter")
    void updateSubscription(List<String> filter) throws Exception {
        RequestObject requestObject = RequestObject.builder()
                .manifest("http://localhost:8000/manifest.dat")
                .filter(filter)
                .build();
        this.mockMvc.perform(
                this.getBuilder(post("/api/v1/fetch"))
                        .content(ow.writeValueAsString(requestObject)))
                .andExpect(status().is(SC_OK));
    }

    @ParameterizedTest
    @MethodSource("filter")
    void updateSubscription$failedWithNullBody() throws Exception {
        this.mockMvc.perform(
                this.getBuilder(post("/api/v1/fetch"))
                        .content(ow.writeValueAsString(null)))
                .andExpect(status().is(SC_BAD_REQUEST));
    }
}
