package co.mizrahi.qfetcher.controllers;

import co.mizrahi.qfetcher.model.RequestObject;
import co.mizrahi.qfetcher.services.QFetcherService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpHeaders;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class QFetcherControllerTest {

    @Mock
    private QFetcherService service;

    @InjectMocks
    private QFetcherController controller;

    @Test
    void fetch() {
        String manifestUrl = "http://test/manifest.dat";
        RequestObject requestObject = RequestObject.builder().manifest(manifestUrl).filter(List.of("json")).build();
        HttpHeaders headers = new HttpHeaders();
        byte[] body = new byte[]{};
        when(service.fetch(requestObject)).thenThrow(new WebClientResponseException(403, "Forbidden", headers,  body, null));
        assertThrows(WebClientResponseException.class, () -> controller.fetch(requestObject).call());
    }
}