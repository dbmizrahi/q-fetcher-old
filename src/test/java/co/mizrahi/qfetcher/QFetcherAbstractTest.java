package co.mizrahi.qfetcher;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@AutoConfigureMockMvc
@AutoConfigureWireMock(stubs = "classpath:/mappings", port = 8000)
@SpringBootTest(
        classes = { QFetcherApp.class },
        webEnvironment = RANDOM_PORT,
        properties = {
                "cloud.vision.key=CLOUD_VISION_KEY",
                "cloud.vision.url=http://localhost:${wiremock.server.port}/v1/images:annotate?key="
        }
)
public abstract class QFetcherAbstractTest {

    @Autowired
    protected MockMvc mockMvc;

    protected static ObjectWriter ow = null;
    protected final static ObjectMapper om = new ObjectMapper();

    public QFetcherAbstractTest() {
        SimpleModule module = new SimpleModule();
        om.registerModule(module);
        ow = om.writer().withDefaultPrettyPrinter();
    }

    protected MockHttpServletRequestBuilder getBuilder(MockHttpServletRequestBuilder builder) {
        return builder
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
    }
}
